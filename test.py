from Matcher import Matcher

artists = Matcher.artistList
print ("%d artists" % len(artists))
nochannels = []
for artist in artists:
	if len(artist['channels']) == 0:
		nochannels.append(artist)
		print artist
print ("%d artists with no channel" % len(nochannels))

multiplechannels = []
for artist in artists:
        if len(artist['channels']) > 1:
                multiplechannels.append(artist)
                print artist
print ("%d artists with multiple channels" % len(multiplechannels))
