#! /usr/bin/python

import sys
import os
import pickle
from pprint import pprint

import sparql

__author__="rik"
__date__ ="$Oct 3, 2014 8:45:58 PM$"

class LinkedBrainzConnector(object):
    def __init__(self):
        self.sparqlEndpoint = "http://linkedbrainz.org/sparql"
        self.sparqlService = sparql.Service(self.sparqlEndpoint)
    
    def getArtistDataFromFile(self, mbid):
        fileName = "data/musicbrainz/%s.mbdata" % mbid
        if not os.path.isfile(fileName):
            return None
        
        data = pickle.load(open(fileName, "rb" ))
        
        return data
    
    def getArtistData(self, mbid):
        """
            Retrieve the artist data including tracks from LinkedBrainz
        """
        
        # try to get the data from file first
        data = self.getArtistDataFromFile(mbid)
        if data:
            print "Loaded data for %s from file." % mbid
            return data

        
        #    statement = ('PREFIX mo: <http://purl.org/ontology/mo/> '
        #        'PREFIX foaf: <http://xmlns.com/foaf/0.1/> '
        #        'SELECT ?name ?member '
        #        'WHERE { '
        #            '?member foaf:name ?name ; '
        #            'mo:member_of ?band . '
        #            '?band foaf:name "Manowar" '
        #            '}'
        #        )

        #    statement = (
        #    'PREFIX mo: <http://purl.org/ontology/mo/> '
        #'PREFIX foaf: <http://xmlns.com/foaf/0.1/> '
        #'PREFIX dc: <http://purl.org/dc/elements/1.1/> '
        #'PREFIX event: <http://purl.org/NET/c4dm/event.owl#> '
        #
        #
        #'SELECT DISTINCT (STR(?track_name) AS ?title) '
        #'WHERE {?track dc:title ?track_name. '
        #'?band foaf:made ?track ; '
        #             'foaf:name "Nirvana" '
        #'}'
        #    )
        

        statement = '''
            PREFIX mo: <http://purl.org/ontology/mo/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX event: <http://purl.org/NET/c4dm/event.owl#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>

            SELECT DISTINCT ?artist_name ?track_name 
            WHERE {?track dc:title ?track_name.
                ?track a <http://purl.org/ontology/mo/Track> .
                <http://musicbrainz.org/artist/'''+ mbid + '''#_> foaf:made ?track ;
                    foaf:name ?artist_name
            }        
        '''

        result = self.sparqlService.query(statement)
        artistName = ""
        trackNames = []
        for row in result.fetchone():
            values = sparql.unpack_row(row)
            artistName = values[0]
            trackNames.append( values[1])
            
        data = {"artist": artistName, "tracks": trackNames}
        
        # save the data to file so we don't have to load it next time.
        print ("Saving data for %s to file" % mbid)
        pickle.dump(data, open("data/musicbrainz/%s.mbdata" % mbid, "wb" ))
        
        return data
    
if __name__ == "__main__":
    connector = LinkedBrainzConnector()
    mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da" # Nirvana
    result = connector.getArtistData(mbid)
    
    pprint(result)