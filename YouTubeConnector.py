#! /usr/bin/python

import os
import sys
import pickle
from pprint import pprint
from optparse import OptionParser

from apiclient.discovery import build

__author__="rik"
__date__ ="$Oct 3, 2014 3:37:17 PM$"

DEVELOPER_KEY = "AIzaSyCsRAE0cZgW7d-Uw7_lbG_O1J-QTC-moKU"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

class YouTubeConnector(object):
    def __init__(self):
        self.developerKey = DEVELOPER_KEY
        self.youtubeAPIServiceName = YOUTUBE_API_SERVICE_NAME
        self.youtubeAPIVersion = YOUTUBE_API_VERSION
        
        self.youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
            developerKey=DEVELOPER_KEY)


    def searchChannels(self, q, maxResults=25):
        search_response = self.youtube.search().list(
            q=q,
            part="id,snippet",
            maxResults=maxResults,
            type="channel"
        ).execute()

        channels = search_response.get("items", [])
        
        return channels
    
    def getChannelDetails(self, channelId):
        response = self.youtube.channels().list(
            part="id, snippet,contentDetails,statistics,topicDetails,invideoPromotion",
            id=channelId
        ).execute()
        
        details = response.get("items", [])
        return details[0]
    
    def getPlaylistVideos(self, playlistId):
        playlistitems_list_request = self.youtube.playlistItems().list(
            part="id,snippet,contentDetails,status",
            playlistId=playlistId,
            maxResults=50
        )
        
        videos = []

        while playlistitems_list_request:
            playlistitems_list_response = playlistitems_list_request.execute()

            # Print information about each video.
            for playlist_item in playlistitems_list_response["items"]:
                videos.append(playlist_item)

            playlistitems_list_request = self.youtube.playlistItems().list_next(
                playlistitems_list_request, playlistitems_list_response)        
        
        return videos
    
    def getArtistChannelsFromFile(self, artistName):
        fileName = "data/youtube/%s.ytdata" % artistName
        if not os.path.isfile(fileName):
            return None
        
        data = pickle.load(open(fileName, "rb" ))
        
        return data
    
    def getArtistChannels(self, artistName, maxChannelResults=10):
        
        
        # try to get the data from file first
        channelsResult = self.getArtistChannelsFromFile(artistName)
        if channelsResult:
            print "Loaded data for %s from file." % artistName
            return channelsResult
        
        channels = self.searchChannels(artistName, maxChannelResults)
        channelsResult = {}
        for channel in channels:
            channelId = channel['snippet']['channelId']
            channelTitle = channel['snippet']['channelTitle']
            channelDetails = self.getChannelDetails(channelId)
            playlistId = channelDetails['contentDetails']['relatedPlaylists']['uploads']

            videos = self.getPlaylistVideos(playlistId)
            channelResult = []
            
            maxVideos = 100
            idx = 0
            for video in videos:
                videoId = video['contentDetails']['videoId']
                videoTitle = video['snippet']['title']
                channelResult.append(videoTitle)
                idx += 1
                if idx == maxVideos:
                    break
                
            channelsResult[channelTitle] = channelResult

        # save the data to file so we don't have to load it next time.
        print ("Saving data for %s to file" % artistName)
        pickle.dump(channelsResult, open("data/youtube/%s.ytdata" % artistName, "wb" ))

        return channelsResult
    
if __name__ == "__main__":
    connector = YouTubeConnector()
    artistName = "U2"
    
    result = connector.getArtistChannels(artistName)
    pprint(result)