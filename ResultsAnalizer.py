#! /usr/bin/python
from __future__ import division

import sys
import os
import pickle
from pprint import pprint

__author__="rik"
__date__ ="$Oct 24, 2014 4:31:47 PM$"

class ResultsAnalyzer(object):
    def __init__(self):
        pass
    
    def analyzeFile(self, fileName=None):
                
        if not os.path.isfile(fileName):
            return None
        
        data = pickle.load(open(fileName, "rb" ))
        
        return self.analyze(data)
    
    def analyze(self, results):
        performances = {}
        threshold = 0
        while threshold <= 1.05:
#            print (threshold)
            performance = self.getPerformance(results, threshold)
#            print(performance)
            performances[round(threshold, 2)] = performance
            threshold += 0.05
        
        return performances
    
    def getPerformance(self, results, threshold):
        recall = 0
        precision = 0
        for artistResult in results:
            correctChannels = artistResult['channels']
            positive = [] # channels marked as correct
            negative = [] # channels marked as incorrect
            for matchResult in artistResult['result']:
                if matchResult['measure'] >= threshold:
                    positive.append(matchResult['channel'])
                else:
                    negative.append(matchResult['channel'])            
            
            correctMatches = []
            for correctChannel in correctChannels:
                if correctChannel in positive:
                    correctMatches.append(correctChannel)
                        
            artistRecall = len(correctMatches)/len(correctChannels) if len(correctChannels) > 0 else 1
            artistPrecision = len(correctMatches)/len(positive) if len(positive) > 0 else 1
            
#            print artistResult['name']
#            print "recall: %f" % artistRecall
#            print "precision: %f" % artistPrecision
            
            recall += artistRecall
            precision += artistPrecision
            
        recall = round(recall/len(results), 2)
        precision = round(precision/len(results), 2)
        
        return (recall, precision)
            
    
if __name__ == "__main__":
    fileName = sys.argv[1]
    analyzer = ResultsAnalyzer()
    results = analyzer.analyzeFile(fileName)
    pprint(results)
    
    print ("Performances Latex style:")
    print("Recall:")
    threshold = 0.0
    while threshold <= 1.00:
        threshold = round(threshold, 2)
        print("(%s, %s)" % (str(threshold), str(round(results[threshold][0], 2))))
        threshold += 0.05
    print("Precision:")
    threshold = 0.0
    while threshold <= 1.00:
        threshold = round(threshold, 2)
        print("(%s, %s)" % (str(threshold), str(round(results[threshold][1], 2))))
        threshold += 0.05
