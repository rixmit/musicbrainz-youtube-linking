#! /usr/bin/python
# -*- coding: utf-8 -*-
 
import os
import sys
import json
import re
from pprint import pprint
import difflib
import itertools
import hashlib
import resource
import pickle
import datetime

from LinkedBrainzConnector import LinkedBrainzConnector
from YouTubeConnector import YouTubeConnector

__author__="rik"
__date__ ="$Oct 6, 2014 9:16:53 PM$"

class Matcher(object):
    
    artistList = [
        { "mbid": "5b11f4ce-a62d-471e-81fc-a69a8278c7da", "name": "Nirvana", "channels": ["NirvanaVEVO", "NirvanaGrunge87"]}, # Nirvana (90s US grunge band)
        { "mbid": "dc894255-3dd4-46d1-9547-fabe79738722", "name": "Chef'Special", "channels": ["Chefspecialmusic"]}, #Chef'Special
        { "mbid": "a3cb23fc-acd3-4ce0-8f36-1e5aa6a18432", "name": "U2", "channels": ["U2VEVO"]}, # U2 (Irish rock band)
        { "mbid": "9282c8b4-ca0b-4c6b-b7e3-4f7762dfc4d6", "name": "Nirvana", "channels": []}, # Nirvana (60s band from the UK)
        { "mbid": "774b509e-15ba-4932-aae4-ec7371e46c27", "name": "André Hazes", "channels": ["andrehazesmusic"]},
        { "mbid": "515fa43e-d3ee-4186-a036-101f39f3be22", "name": "André Hazes jr.", "channels": ["ItsJustMeSinging"]},
        { "mbid": "88bdc8b5-b254-40f4-b626-434332081c05", "name": "Frans Duijts", "channels": ["fransduijts"]},
        { "mbid": "e0140a67-e4d1-4f13-8a01-364355bee46e", "name": "Justin Bieber", "channels": ["JustinBieberVEVO", "xJustinBieberLyrics", "JustinBieberFenty"]},
        { "mbid": "5f9f2dfb-76f0-4872-ad7d-f9d84a908cb5", "name": "Both", "channels": []}, # Both (Duluth, MN, USA)
        { "mbid": "0781a3f3-645c-45d1-a84f-76b4e4decf6d", "name": "Both", "channels": []}, # Both (France)
        { "mbid": "0fc3ed5c-eeb7-45b4-b222-132dae4834ef", "name": "Jebroer", "channels": ["jebroertv"]},
        { "mbid": "9b62a8a3-fcd5-4b51-a970-0f08a440ed6d", "name": "Gramatik", "channels": ["Gramatikk", "gramatikcollection"]},
        { "mbid": "0ab49580-c84f-44d4-875f-d83760ea2cfe", "name": "Maroon 5", "channels": ["Maroon5VEVO", "TheBurle2011"]},
        { "mbid": "a466c2a2-6517-42fb-a160-1087c3bafd9f", "name": "Slipknot", "channels": ["slipknot"]}, # Slipknot (US metal band)
        { "mbid": "ceb7b6be-1dbb-4d80-b157-5588bc884764", "name": "Slipknot", "channels": []}, # Slipknot (Late-80's punk band)
        { "mbid": "534dda3c-b73f-408b-8889-bd68eae84df6", "name": "Ben Howard", "channels": ["benhowardmusic", "BenHowardVEVO"]},
        { "mbid": "186e216a-2f8a-41a1-935f-8e30c018a8fe", "name": "Passenger ", "channels": ["passengermusic"]}, # Passenger (British singer-songwriter Mike Rosenberg, formerly of the band /Passenger.)
        { "mbid": "b071f9fa-14b0-4217-8e97-eb41da73f598", "name": "The Rolling Stones", "channels": ["TheRollingStones", "TheRollingStonesVEVO"]},
        { "mbid": "b10bbbfc-cf9e-42e0-be17-e2c3e1d2600d", "name": "The Beatles", "channels": ["thebeatles", "TheBeatles19501", "BeatlesHD"]},
        { "mbid": "0096e98d-7dc7-437a-957a-6719ff1a0f20", "name": "Normaal", "channels": []},
        { "mbid": "24f8d8a5-269b-475c-a1cb-792990b0b2ee", "name": "Rancid", "channels": ["RancidNihilism"]}, # Rancid (Berkeley, California punk band)
        { "mbid": "cc197bad-dc9c-440d-a5b5-d52ba2e14234", "name": "Coldplay", "channels": ["ColdplayVEVO", "ColdplayMnFans", "VivaColdplay042"]},
        { "mbid": "dcad1062-476a-4bde-beb9-391ac4ad5eaa", "name": "Di-rect", "channels": ["DirectVEVO"]},
        { "mbid": "234bab50-2f04-4d21-8f91-df93ef554855", "name": "Mr. Scruff", "channels": ["mrscruffofficial"]},
        { "mbid": "0837d2bc-ceb6-4512-9490-b30f5c0390fa", "name": "Chinese Man", "channels": ["chinesemanrec"]},
        { "mbid": "72c536dc-7137-4477-a521-567eeb840fa8", "name": "Bob Dylan", "channels": ["BobDylanVEVO"]}, # 
        { "mbid": "51f78044-3474-4f7a-88ae-9dc56f95e747", "name": "Cuby + Blizzards", "channels": []}, # 
        { "mbid": "efa2c11a-1a35-4b60-bc1b-66d37de88511", "name": "DJ Shadow", "channels": ["DJShadowVEVO", "djshadow"]}, # 
        { "mbid": "a1f8f29c-4163-48d1-9469-35f74e61ee4e", "name": "Guts", "channels": []}, # Guts (French abstract hip-hop producer)
        { "mbid": "2ef6cec2-fee0-40d9-9107-2ce5e7f4ecd8", "name": "Guus Meeuwis", "channels": ["guusmeeuwis247"]}, # 
        { "mbid": "f9dcefc1-1b1c-47d3-b230-16d432de79ab", "name": "Nielson", "channels": ["NielsonMusic"]}, # Nielson (Dutch singer Niels Littooij)
        { "mbid": "b2029169-2574-4305-820f-252a5fde3697", "name": "Meghan Trainor", "channels": []}, # 
        { "mbid": "2c5e2a98-d9ea-46b5-9618-d9bb4863a920", "name": "George Ezra", "channels": ["GeorgeEzraMusic", "GeorgeEzraVEVO"]}, # 
        { "mbid": "5e8da504-c75b-4bf5-9dfc-119057c1a9c0", "name": "Anouk", "channels": ["anouktv"]}, # Anouk (Dutch rock singer)
        { "mbid": "a633117d-dc2b-4ae5-8906-6670584c2e07", "name": "Anouk", "channels": []}, # Anouk (French R*B artist)
        { "mbid": "c9dd033c-0270-463c-b3ec-f4b7712486fe", "name": "Rita Ora", "channels": ["ORAbyRitaOra", "OfficialOra", "RitaOraVEVO", "RitaOraMTV", ""]}, # 
#        { "mbid": "", "name": "", "channels": []}, # 
    ]
    
    def __init__(self):
        # set memory limits
#        soft, hard = 10**8, 10**8
#        resource.setrlimit(resource.RLIMIT_AS,(soft, hard))
        
        self.lbConnector = LinkedBrainzConnector()
        self.ytConnector = YouTubeConnector()
        pass
    
    def match2(self, mbid):
        
        # get the MusicBrainz data
        print("Loading MusicBrainz data...")
        mbData = self.lbConnector.getArtistData(mbid)
        print("done")
#        mbData = {
#            "artist": "Both",
#            "tracks": [
#                "Simple Exercise",
#                "En attendant d'aller sur Mars",
#            ]
#        }
        
        # get the YouTube data
        mbArtistName = mbData['artist']
        mbTracks = mbData['tracks']
        if len(mbTracks) == 0:
            print("No MB tracks found")
            
        print("%d tracks found for artist %s" % (len(mbTracks), mbArtistName))
        
        cleanedTracks = []
        for mbTrack in mbTracks:
            mbTrack = self.cleanTitle(mbTrack)
            if mbTrack:
                cleanedTracks.append(mbTrack)
        
        mbTracks = cleanedTracks
        
        print("Loading YouTube data...")
        ytData = self.ytConnector.getArtistChannels(mbArtistName)
        print("%d channels found for artist %s" % (len(ytData), mbArtistName))
        print("done")
#        ytData = {
#            "Both": [
#                "Both - Simple Exercise",
#                "Both - En attendant d'aller sur Mars..."
#            ],
#            "both": [
#                "Both - The Inevitable Phyllis"
#            ]
#        }
        
        results = []
        
        mbArtistNameLower = mbArtistName.lower()

        sequenceMatcher = difflib.SequenceMatcher()
        
        for channelTitle, channelVideos in ytData.items(): # loop over all channels
            if len(channelVideos) <= 5:
                print("Too few videos...")
                continue
            print("Processing channel %s" % channelTitle)
            maxVideoSimilarities = []
            
            # use only the first x videos
            channelVideos = channelVideos[:20]
            
            # find the best matching mbTracks for each video
            for channelVideo in channelVideos:
                print("Processing video %s" % channelVideo)
                channelVideoCleaned = channelVideo
                channelVideoCleaned = channelVideoCleaned.lower()
                channelVideoCleaned = self.cleanTitle(channelVideoCleaned)
                # remove mb artist name and separating dash from yt video title
#                channelVideoCleaned = channelVideoCleaned.replace(mbArtistNameLower, "")
#                channelVideoCleaned = channelVideoCleaned.replace(" - ", "")                
                
                sequenceMatcher.set_seq2(channelVideoCleaned) # set the second sequence once. This increases computation speed later
                maxSimilarity = 0
                for mbTrack in mbTracks:
                    mbTrack = mbTrack.lower()
                    # prepend mb artist name to track to match common format of yt video title format
                    mbTrack = "%s - %s" % (mbArtistNameLower, mbTrack)
                    
                    sequenceMatcher.set_seq1(mbTrack)
                    maxSimilarity = max(maxSimilarity, sequenceMatcher.ratio())
            
                maxVideoSimilarities.append(maxSimilarity)
            
            # if there are only few mbTracks (less than videos) remove the wordt similarities
            diff = len(channelVideos) - len(mbTracks)
            if diff > 0:
                maxVideoSimilarities = self.trimWorstSimilarities(maxVideoSimilarities, diff)
            
            if len(maxVideoSimilarities) == 0:
                channelMeasure = 0
            else:
                channelMeasure = sum(maxVideoSimilarities)/len(maxVideoSimilarities)
            results.append({"channel": channelTitle, "videos": len(channelVideos), "measure": channelMeasure})
            print(channelMeasure)
            print("done")
            
        return results
        
    def trimWorstSimilarities(self, similarities, count):
        similarities = sorted(similarities)
        similarities = similarities[count:]
        
        return similarities
    
    def match(self, mbid):
        
        # get the MusicBrainz data
        mbData = self.lbConnector.getArtistData(mbid)
#        mbData = {
#            "artist": "Both",
#            "tracks": [
#                "Simple Exercise",
#                "En attendant d'aller sur Mars",
#            ]
#        }
        
        # get the YouTube data
        mbArtistName = mbData['artist']
        ytData = self.ytConnector.getArtistChannels(mbArtistName)
#        ytData = {
#            "Both": [
#                "Simple Exercise",
#                "En attendant d'aller sur Mars..."
#            ],
#            "both": [
#                "The Inevitable Phyllis"
#            ]
#        }
        
        mbTracks = mbData['tracks']
        for channelTitle, channelVideos in ytData.items(): # loop over all channels
            pairsHashMap = self.getPairsHashMap(mbTracks, channelVideos)
            similarityMap = []
            print (len(mbTracks))
            print (len(channelVideos))
            graphMappings = self.graphCombinations(range(0, len(mbTracks)), range(0, len(channelVideos)))
            print (len(graphMappings))

            logEveryN = 10000
            i = 0
            
            for graphMapping in graphMappings:
                measure = 0
                for pair in graphMapping:
                    mbTrackName = mbTracks[pair[0]]
                    ytVideoTitle = channelVideos[pair[1]]
                    # add the artist name to the track name (this is usually how YouTube titles are respresented: artist - trackname)
#                    fullMbTrackName = mbArtistName + " - " + mbTrackName
#                    similarity = max(self.similarity(mbTrackName, ytVideoTitle), self.similarity(fullMbTrackName, ytVideoTitle))
#                    measure += similarity
                    measure += pairsHashMap[self.hash(mbTrackName+ytVideoTitle)]
                    
                measure = measure/len(graphMapping)
                similarityMap.append({
                    'graph': graphMapping,
                    'measure': measure
                })
                
                i += 1
                if (i % logEveryN) == 0:
                    print(i)


            # order the similarity map by measure descending
            orderedSimilarityMap = sorted(similarityMap, key=lambda k: k['measure'], reverse=True)
            
            print ("Channel title: %s" % channelTitle)
            print ("Best matches: ")
            maxResults = 5
            idx = 0
            for graph in orderedSimilarityMap:
                pprint(graph)
                idx += 1
                if idx == maxResults:
                    break
        
            
    
    
    def getPairsHashMap(self, mbTracks, ytVideos):
        pairs = list(itertools.product(mbTracks, ytVideos))
        
        hashMap = {}
        for pair in pairs:
            hashMap[self.hash(pair[0]+pair[1])] = self.similarity(*pair)
            
        return hashMap
    
    def hash(self, string):
        return hashlib.md5(string.encode('utf-8')).hexdigest()
    
        
        
    def similarity(self, mbTrack, channelVideo):
        """
            Find the similarity between Musicbrainz track and YouTube video
        """
                
        ratio = difflib.SequenceMatcher(None, mbTrack, channelVideo).ratio()

        return ratio
    
    def cleanTitle(self, title):
        """
            Clean the given title string from excess data.
            Currently removes everything between (), [] and {} (including these enclosing characters)
        """
        
        # remove enclosures (...), [...] and {...}
        enclosures = self.findEnclosures(title)
        for enclosure in enclosures:
            title = title.replace(enclosure, '')
        title = title.strip()
        
        return title
    
        
    def findEnclosures(self, titleString):
        m = re.findall(r"\[[^]^[]*\]|\([^)^(]*\)|\([^}^{]*\)", titleString)
        result = ""
        if m:
            result = m

        return result
    
    def graphCombinations(self, list1, list2):
        """
            Find all combinations of two lists
            [[1, 2], [3, 4]] = [[[1, 3], [2, 4]], [[1, 4], [2, 3]]]
        """
        
        if len(list1) > len(list2):
            combinations = [zip(x,list2) for x in itertools.permutations(list1,len(list2))]
        else:
            combinations = [zip(list1, x) for x in itertools.permutations(list2,len(list1))]

        return combinations
            
        
    
if __name__ == "__main__":
    matcher = Matcher()

#    mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da" # Nirvana
#    mbid = "dc894255-3dd4-46d1-9547-fabe79738722" # Chef'Special
#    result = matcher.match2(mbid)
#    pprint(result)
#    sys.exit()
    
    results = []
    for artist in matcher.artistList:
        artist['result'] = matcher.match2(artist['mbid'])
        results.append(artist)
        
        print ("Saving results for %s to file" % artist['mbid'])
        pickle.dump(artist, open("data/results/%s.result" % artist['mbid'], "wb" ))
        
    print ("Saving end results for to file")
    pickle.dump(results, open("data/endresults/endresult-%s.result" % datetime.datetime.now().strftime("%Y%m%d%H%M%S"), "wb" ))
    print("End results:")
    pprint(results)
    
#    print ("Latex rows:")
#    for result in results:
#        row = "%s (%s) & " % (result['name'], result['mbid'])
#        for correctChannel in result['channels']:
#            row += "%s, " % correctChannel
#        row = row[:-2]
#        row += " & "
#        for foundChannel in result['result']:
#            row += "%s (%s), " % (foundChannel['channel'], str(round(foundChannel['measure'], 2)))
#        row = row[:-2]
#        row += " \\\\ \\hline"
#        print (row)
#    print(json.dumps(results))
        
    
    
    
    
    