#!/usr/bin/python

from pprint import pprint
from optparse import OptionParser

from apiclient.discovery import build

# Set DEVELOPER_KEY to the "API key" value from the "Access" tab of the
# Google APIs Console http://code.google.com/apis/console#access
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "AIzaSyCsRAE0cZgW7d-Uw7_lbG_O1J-QTC-moKU"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def youtube_search(options):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
        q=options.q,
        part="id,snippet",
        maxResults=options.maxResults,
        type="channel"
    ).execute()

    channels = []

    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#channel":
            channels.append("%s (%s)" % (search_result["snippet"]["title"],
                                   search_result["id"]["channelId"]))

    print "Channels:\n", "\n".join(channels), "\n"


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--q", dest="q", help="Search term",
        default="Google")
    parser.add_option("--max-results", dest="maxResults",
        help="Max results", default=25)
    (options, args) = parser.parse_args()

    youtube_search(options)
